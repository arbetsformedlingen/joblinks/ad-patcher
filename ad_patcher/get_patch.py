import json
from loguru import logger
import requests
import settings


def load_patch_file() -> dict:
    try:
        response = requests.get(settings.PATCH_FILE_URL)
        response.raise_for_status()
        json_response = json.loads(response.content)
        logger.info(f"Got patch data from {settings.PATCH_FILE_URL}")
        logger.debug(json_response)
        return json_response
    except Exception as e:
        logger.error(f"Could not load patch data. Error: {e}")
        return {}
