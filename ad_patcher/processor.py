from loguru import logger
from ad_patcher import get_patch


def patch_ad(ad: dict, patch: dict) -> dict:
    try:
        if patch_for_ad := patch.get(ad["id"], None):
            logger.info(f"Found patch for ad {ad['id']} ")
            change_list = patch_for_ad.get("change_list", [])
            for change in change_list:
                key = change["key"]
                value = change["value"]
                ad[key] = value
                logger.info(f"Added {key}:{value} to {ad['id']}")
    except Exception as e:
        logger.error(f"Could not patch ad {ad['id']}, error: {e}")
    return ad


def process_all_ads(all_ads):
    logger.info("start processing ads")
    converted_ads = []
    patch_data = get_patch.load_patch_file()
    for ad in all_ads:
        converted_ads.append(patch_ad(ad, patch_data))
    logger.info(f"Processed {len(converted_ads)} ads")
    return converted_ads
