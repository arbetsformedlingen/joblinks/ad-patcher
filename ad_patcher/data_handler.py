import sys
import json
import ndjson
from datetime import datetime
import settings
from loguru import logger


class DataHandler:
    """
    Opens the file or reads ads from stdin at start
    Prints all ads to stdout at exit (depending on environment variable)
    settings: see settings.py

    usage:
    with DataHandler as dh:
        all_ads = dh.ads
    """

    def __init__(self):
        self.start = datetime.now()
        self.file_name = settings.READ_FROM_FILE_NAME
        self.ads = self._get_data()
        self.number_of_ads = len(self.ads)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.print_ads()
        self.check_number_of_ads()
        logger.info(f"Completed. number of ads: {len(self.ads)}. Total time: {datetime.now() - self.start}")

    def _get_data(self):
        if settings.USE_STDIN:
            return self._read_ads_from_std_in()
        else:
            return self._read_from_file()

    def _read_ads_from_std_in(self):
        return ndjson.load(sys.stdin)

    def _read_from_file(self):
        start = datetime.now()
        data = []
        with open(self.file_name, 'r', encoding='utf-8') as data_file:
            for item in data_file.readlines():
                data.append(json.loads(item))
        logger.info(f"Completed loading of: {len(data)} ads. Time used: {datetime.now() - start}")
        return data

    def print_ads(self):
        if not settings.PRINT:
            logger.info("Will not print ads")
            return
        logger.info(f"starting print to stdout of {len(self.ads)} ads")
        for ad in self.ads:
            output_json = ndjson.dumps([ad], ensure_ascii=False)
            print(output_json, file=sys.stdout)

    def print_and_log_error(self, error_msg):
        logger.error(error_msg)
        print(error_msg, file=sys.stderr)

    def check_number_of_ads(self):
        if self.number_of_ads != len(self.ads):
            logger.error(
                f"Wrong number of ads after processing, expected: {self.number_of_ads}, actual: {len(self.ads)}. Exit!")
            sys.exit(1)
