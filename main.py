from ad_patcher.data_handler import DataHandler
from ad_patcher.chaos import ChaosTest
from ad_patcher import processor


def run():
    with DataHandler() as dh, ChaosTest():
        dh.ads = processor.process_all_ads(dh.ads)


if __name__ == '__main__':
    run()
