import pytest


@pytest.fixture()
def patch_for_tests() -> dict:
    return {
        "example_id": {
            "change_list": [
                {
                    "key": "my_key",
                    "value": "my_value",
                    "date": "2023-03-03",
                    "comment": "'key' is set to 'value' in the ad. Other fields (like this one) are ignored"
                },
                {
                    "key": "next_key",
                    "value": "my_value",
                    "date": "2023-03-03",
                    "comment": "multiple changes in one ad"
                }
            ]
        },
        "123": {
            "change_list": [
                {
                    "key": "my_key",
                    "value": "my_value",
                    "date": "2023-03-03"
                },
                {
                    "key": "my_other_key",
                    "value": "something",
                    "date": "2023-03-08"
                }
            ]
        },
        "456": {
            "change_list": [
                {
                    "key": "my_key2",
                    "value": "my_value2"
                }
            ]
        },
        "150": {
            "change_list": [
                {
                    "key": "nested",
                    "value": {
                        "nested_key": "my_nested_key",
                        "value": {
                            "nested_level_2": "a deeply nested value"
                        }
                    }
                }
            ]
        },
        "111": {
            "change_list": [
                {
                    "key": "workplace_addresses",
                    "value": {
                        "municipality_concept_id": "???",
                        "municipality": "Ekeby",
                        "region_concept_id": "???",
                        "region": "Skåne län",
                        "country_concept_id": "i46j_HmG_v64",
                        "country": "Sverige"
                    }
                }
            ],
            "comment": "INC1234, added 2023-03-08"
        }
    }
