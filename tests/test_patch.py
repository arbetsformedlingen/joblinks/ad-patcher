import pytest

from ad_patcher.processor import patch_ad

ads = [
    ({"id": "123", "some_text": "Lorem Ipsum"},
     {"id": "123", "some_text": "Lorem Ipsum",
      "my_key": "my_value",
      "my_other_key": "something"}),
    ({"id": "9999", "comment": "not included in patch file"},
     {"id": "9999", "comment": "not included in patch file"}),
    ({"id": "111", "comment": "multiple"},
     {"id": "111", "comment": "multiple",
      "workplace_addresses": {
          "municipality_concept_id": "???",
          "municipality": "Ekeby",
          "region_concept_id": "???",
          "region": "Skåne län",
          "country_concept_id": "i46j_HmG_v64",
          "country": "Sverige"
      }}),
]


@pytest.mark.parametrize("ad, expected", ads)
def test_patch(ad, expected, patch_for_tests):
    converted = patch_ad(ad, patch_for_tests)
    for key, value in expected.items():
        assert converted[key] == value
