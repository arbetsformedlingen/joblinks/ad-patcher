# ad-patcher-processor

A way of correcting ads when scraping or classification has gone wrong.  
It takes a json file from the [ad-patcher-data repository](https://gitlab.com/arbetsformedlingen/joblinks/ad-patcher-data) and applies the changes listed for a particular ad id.
The json file has entries with ad id and new data (key:value) which will overwrite the original values.


## Prerequisites
Python version 3.11.* or higher is required

### Poetry is used or dependency management
[Poetry documentation](https://python-poetry.org/docs)
`pip install poetry`
Set up the project as a Poetry project in your editor and installation in a virtual environment should be handled.
Or run `poetry install --with tests` which will create a virtual environment and install dependencies from pyproject.toml

## Tests
Run `pytest tests`


# Environment variables:

`USE_STDIN`, default value True, used as part of the JobAd Links pipeline.  
`FILE_NAME`, default value 'output.json', used if USE_STDIN is False.     
`LOGURU_LEVEL`, default value 'INFO', log level.  
`PRINT`, default value True. Determines if ads should be written to stdout at completion.    
`PATCH_FILE_URL`, an url to a json file with desired corrections. Default value points to a file in the [ad-patcher-data](https://gitlab.com/arbetsformedlingen/joblinks/ad-patcher-data/-/raw/main/patch_data.json) repository.  


# Usage
Set values for environment variables if needed
`python main.py`  
optional argument `--chaos-test` will cause 5% risk of exiting immediately with error code 1.


# Run locally with your own patch file in json format for development:
Start Pýthon's http server in a directory with the desired json file `patch.json`in this example. In the example it will listen to port 9000

`python -m http.server 9000`

and the patch file should be accessible on http://localhost:9000/patch.json which can be used in the environment variable `PATCH_FILE_URL`


# Docker / Podman
To separate runtime code and dependencies from tests, the Dockerfile is split into three sections.
- Base: Code and runtime dependencies
- Test: Tests are copied here, Pytest is installed and tests are executed
- Runtime

Build command from the repo root: 
`podman build -t ad-patcher .` 

When tests have executed successfully in the `test`section, a file called`/.tests-successful` is created.
The next section of the Dockerfile will try to copy this file, if tests were successful the file is copied. Otherwise the COPY operation will fail because there is no such file and the build will fail.
