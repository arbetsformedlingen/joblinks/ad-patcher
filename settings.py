import os
from distutils.util import strtobool

USE_STDIN = strtobool(os.getenv("USE_STDIN", default='true'))
READ_FROM_FILE_NAME = os.environ.get("FILE_NAME", default='output.json')
LOGURU_LEVEL = os.environ.get("LOG_LEVEL", default="INFO")
PRINT = strtobool(os.getenv("PRINT", default='true'))

# Link to a json file with changes
PATCH_FILE_URL = os.getenv("PATCH_FILE_URL",
                           "https://gitlab.com/arbetsformedlingen/joblinks/ad-patcher-data/-/raw/main/patch_data.json")
