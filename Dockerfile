FROM docker.io/library/alpine:3.19.2 as base

WORKDIR /app
COPY main.py  poetry.lock  pyproject.toml  run.sh  settings.py  README.md /app/
COPY ad_patcher   /app/ad_patcher/

RUN apk update && apk add python3 py3-pip scons build-base patchelf &&\
    python -m venv venv &&\
    . venv/bin/activate && python -m pip install poetry wheel &&\
    . venv/bin/activate && python -m pip install staticx pyinstaller &&\
    . venv/bin/activate && python -m poetry config virtualenvs.create false &&\
    . venv/bin/activate && python -m poetry install &&\
    . venv/bin/activate && pyinstaller -F main.py &&\
    . venv/bin/activate && staticx dist/main main &&\
    strip main &&\
    rm -rf /tmp/*



###############################################################################
# test
FROM base AS test
# When tests have run successfully a file is created.
# In the next step this file is copied, if the tests weren't successful and no file was created,
# the build will fail.

COPY tests/ tests/

# Only Pytest is installed, all other dependcies were installed in the base section
RUN . venv/bin/activate \
    && python -m poetry install --with tests


RUN  . venv/bin/activate \
     && python -m pytest -v tests \
     && ./main \
     && touch /.tests-successful


###############################################################################
# Runtime
FROM scratch

# If tests weren't successful the copy will fail so that the build fails
COPY --from=test /.tests-successful /
COPY --from=base /app/main .
COPY --from=base /tmp /tmp

ENTRYPOINT ["./main"]
